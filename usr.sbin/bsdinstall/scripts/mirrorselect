#!/bin/sh
#-
# Copyright (c) 2011 Nathan Whitehorn
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $FreeBSD$

BSDCFG_SHARE="/usr/share/bsdconfig"
. $BSDCFG_SHARE/common.subr || exit 1

: ${BSDDIALOG_OK=0}
: ${BSDDIALOG_CANCEL=1}
: ${BSDDIALOG_HELP=2}
: ${BSDDIALOG_EXTRA=3}
: ${BSDDIALOG_ESC=5}
: ${BSDDIALOG_ERROR=255}

case `uname -r` in
13.*)
	_hbsd_dist_dir="pub/hardenedbsd/13-stable/`uname -m`/`uname -p`/BUILD-LATEST"
	;;
12.*)
	_hbsd_dist_dir="pub/hardenedbsd/12-stable/`uname -m`/`uname -p`/BUILD-LATEST"
	;;
*)
	_hbsd_dist_dir="pub/hardenedbsd/current/`uname -m`/`uname -p`/BUILD-LATEST"
	;;
esac

exec 3>&1
MIRROR=`bsddialog --backtitle "$OSNAME Installer" \
    --title "Mirror Selection" --extra-button --extra-label "Other" \
    --menu "Please select the site closest to you or \"other\" if you'd like to specify a different choice.  Also note that not every site listed here carries more than the base distribution kits. Only Primary sites are guaranteed to carry the full range of possible distributions. Select a site that's close!" \
    0 0 16 \
	https://ci-01.nyi.hardenedbsd.org	"Main Site (NYI)"\
	http://qspcqclhifj3tcpojsbwoxgwanlo2wakti2ia4wozxjcldkxmw2yj3yd.onion "Onion Service" \
    2>&1 1>&3`
MIRROR_BUTTON=$?
exec 3>&-

BSDINSTALL_DISTSITE="$MIRROR/${_hbsd_dist_dir}"


case $MIRROR_BUTTON in
$BSDDIALOG_ERROR | $BSDDIALOG_CANCEL | $BSDDIALOG_ESC)
	exit 1
	;;
$BSDDIALOG_OK)
	;;
$BSDDIALOG_EXTRA)
	exec 3>&1
	BSDINSTALL_DISTSITE=`bsddialog --backtitle "$OSNAME Installer" \
	    --title "Mirror Selection" \
	    --inputbox "Please enter the URL to an alternate $OSNAME mirror:" \
	    0 74 "$BSDINSTALL_DISTSITE" 2>&1 1>&3`
	MIRROR_BUTTON=$?
	exec 3>&-
	test $MIRROR_BUTTON -eq $BSDDIALOG_OK || exec $0 $@
	;;
esac

export BSDINSTALL_DISTSITE
echo $BSDINSTALL_DISTSITE >&2
